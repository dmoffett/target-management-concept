import {Injectable} from '@angular/core';
import {Client} from '../model/client.class';
import {Site} from '../model/site.class';
import {LOB} from '../model/lob.class';
import {Target} from '../model/target.class';
import {KPI} from '../model/kpi.class';
import {TargetHistory} from "../model/target-history.class";

@Injectable()
export class TargetsService {
  /*
  Some mock data
   */
  cNike = new Client('Nike');
  cAnthem = new Client('Anthem');
  clients = [
    this.cNike,
    this.cAnthem
  ];

  sWilsonville = new Site('Wilsonville');
  sFortCollins = new Site('Fort Collins');
  sBoulder = new Site('Boulder');
  sDenver = new Site('Denver');

  nikeSites = [
    this.sWilsonville,
    this.sFortCollins
  ];

  anthemSites = [
    this.sBoulder,
    this.sDenver
  ];

  lShoes = new LOB('Shoes');
  lPants = new LOB('Pants');
  lInsurance = new LOB('Insurance');
  lPandas = new LOB('Pandabears');

  nikeLOBs = [
    this.lShoes,
    this.lPants
  ];

  anthemLOBs = [
    this.lInsurance,
    this.lPandas
  ];

  kABS = new KPI('ABS');
  kCSAT = new KPI('CSAT');
  kFCR = new KPI('FCR');
  kpis = [
    this.kABS,
    this.kCSAT,
    this.kFCR,
  ];

  // Dictionaries (for easy randomization)
  clientSites = {
    [this.cNike.name]: this.nikeSites,
    [this.cAnthem.name]: this.anthemSites
  };

  clientLOBs = {
    [this.cNike.name]: this.nikeLOBs,
    [this.cAnthem.name]: this.anthemLOBs
  };


  targetHistories = [];


  constructor() {
    // Randomize some targets
    for (const client of this.clients) {
      for (const site of this.clientSites[client.name]) {
        for (const lob of this.clientLOBs[client.name]) {
          for (const kpi of this.kpis) {
            // Create a new history set
            const history = new TargetHistory(client, site, lob, kpi);
            this.targetHistories.push(history);

            // Randomize a number of historical changes
            const num = Math.floor(Math.random() * 5) + 1;
            let previousDay = 1; // Hack for ascending dates
            for (let i = 0; i < num; i++) {
              // Randomize the target value and day
              const targetValue = Math.round(Math.random() * 50) + 50;
              previousDay = Math.floor(Math.random() * 60) + previousDay;
              const target = new Target();
              target.target = targetValue;
              target.day = previousDay;
              target.client = client;
              target.site = site;
              target.lob = lob;
              target.kpi = kpi;
              history.targets.push(target);
            }
          }
        }
      }
    }
  }

  getTargets() {
    return this.targetHistories;
  }

  getClients() {
    return this.clients;
  }

  getClientSites() {
    return this.clientSites;
  }

  getClientLOBs() {
    return this.clientLOBs;
  }

  getKPIs() {
    return this.kpis;
  }

}
