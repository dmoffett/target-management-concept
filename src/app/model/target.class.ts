import {Client} from './client.class';
import {Site} from './site.class';
import {LOB} from './lob.class';
import {KPI} from './kpi.class';

export class Target {
  public target: number;
  public day: number;
  public client: Client;
  public site: Site;
  public lob: LOB;
  public kpi: KPI;

  constructor() {

  }
}
