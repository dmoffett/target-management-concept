import {Client} from './client.class';
import {Site} from './site.class';
import {LOB} from './lob.class';
import {Target} from './target.class';
import {KPI} from './kpi.class';

export class TargetHistory {
  public targets: Array<Target> = [];

  constructor(public client: Client, public site: Site, public lob: LOB, public kpi: KPI) {

  }
}
