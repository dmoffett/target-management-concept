import {Component, OnInit} from '@angular/core';
import {TargetsService} from './services/targets.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Targets Management Concept';

  targetHistories;
  clients;
  clientSites;
  clientLOBs;
  kpis;

  constructor(private targetsService: TargetsService) {
  }

  ngOnInit() {
    this.targetHistories = this.targetsService.getTargets();
    this.clients = this.targetsService.getClients();
    this.clientSites = this.targetsService.getClientSites();
    this.clientLOBs = this.targetsService.getClientLOBs();
    this.kpis = this.targetsService.getKPIs();
  }
}
