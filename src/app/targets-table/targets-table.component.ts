import {Component, Input, OnChanges, OnInit, ViewChild} from '@angular/core';
import {isNullOrUndefined} from 'util';
import {Target} from '../model/target.class';
import {BaseChartDirective} from 'ng2-charts';

@Component({
  selector: 'app-targets-table',
  templateUrl: './targets-table.component.html',
  styleUrls: ['./targets-table.component.css']
})
export class TargetsTableComponent implements OnInit, OnChanges {
  @Input() targetHistories;
  @Input() clients;
  @Input() clientSites;
  @Input() clientLOBs;
  @Input() kpis;


  validSites = [];
  validLOBs = [];

  filteredHistories = [];
  filteredTargets = [];

  // These are state variables and break one-way data flow paradigm; but since
  // this is a fast-and-loose demo, and I don't have a store, this is how we're doing this.
  selectedClient;
  selectedSite;
  selectedLOB;
  selectedKPI;

  // Variables for creating a new target
  newTargetDay;
  newTarget;

  // Variable for enabling/disabling new target form
  newTargetFormEnabled = false;

  // Chart
  @ViewChild('baseChart') chart: BaseChartDirective;
  public lineChartData: Array<any>;
  public lineChartLabels: Array<any>;
  public lineChartOptions: any = {
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          steps: 10,
          stepValue: 10,
          min: 0,
          max: 100
        }
      }]
    }
  };
  public lineChartColors: Array<any>;
  public lineChartLegend = true;
  public lineChartType = 'line';

  constructor() {
    // Init our label axis (days)
    const xAxis = [];
    for (let i = 1; i <= 365; i++) {
      xAxis.push('Day ' + i);
    }
    this.lineChartLabels = xAxis;
  }

  ngOnInit() {
  }

  ngOnChanges() {
    // Select firsts from list
    this.selectedClient = this.clients[0];
    this.validate();
    this.selectedSite = this.clientSites[this.selectedClient.name][0];
    this.selectedLOB = this.clientLOBs[this.selectedClient.name][0];

    this.filter();
  }

  validate() {
    if (isNullOrUndefined(this.selectedClient)) {
      this.validSites = [];
      this.validLOBs = [];
    } else {
      this.validSites = this.clientSites[this.selectedClient.name];
      this.validLOBs = this.clientLOBs[this.selectedClient.name];
    }

    // Reset selections every time client is changed
    this.selectedSite = null;
    this.selectedLOB = null;
    this.selectedKPI = null;
  }

  filter() {
    // Reset filtered results
    const filtered = [];
    const filteredHistories = [];

    for (const history of this.targetHistories) {
      const matchesClient = isNullOrUndefined(this.selectedClient) ? true : history.client === this.selectedClient;
      const matchesSite = isNullOrUndefined(this.selectedSite) ? true : history.site === this.selectedSite;
      const matchesLOB = isNullOrUndefined(this.selectedLOB) ? true : history.lob === this.selectedLOB;
      const matchesKPI = isNullOrUndefined(this.selectedKPI) ? true : history.kpi === this.selectedKPI;
      if (matchesClient && matchesSite && matchesLOB && matchesKPI) {
        // Add the target history to the filtered histories list
        filteredHistories.push(history);

        // Add all the target to the filtered targets list
        for (const target of history.targets) {
          filtered.push(target);
        }
      }
    }

    // Sort
    filtered.sort((t1, t2) => {
      if (t1.day > t2.day) {
        return 1;
      } else if (t1.day < t2.day) {
        return -1;
      }
      return 0;
    });

    this.filteredTargets = filtered;
    this.filteredHistories = filteredHistories;

    // New targets can only be added when a client, site, lob, and kpi are selected
    this.newTargetFormEnabled = !(
      isNullOrUndefined(this.selectedClient) ||
      isNullOrUndefined(this.selectedSite) ||
      isNullOrUndefined(this.selectedLOB) ||
      isNullOrUndefined(this.selectedKPI));

    this.transformChartData();
  }

  transformChartData() {
    // Create our data sets
    const datasets = [];
    for (const history of this.filteredHistories) {
      const data = [];
      const label = history.client.name + '-' + history.site.name + '-' + history.lob.name + '-' + history.kpi.name;

      // Interpolate...kuldgey
      for (let i = 1; i <= 365; i++) {
        const targetOnDay = this.getTargetForDay(i, history.targets);
        if (!isNullOrUndefined(targetOnDay)) {
          data.push(targetOnDay.target);
        } else {
          data.push(null);
        }
      }
      datasets.push({data: data, label: label});
    }

    this.lineChartData = datasets.length > 0 ? datasets : null;
    this.reloadChart();
  }

  getTargetForDay(day: number, targets: Array<Target>) {
    // We need to find the target with the greatest day where target.day <= day
    let out = null;
    for (const target of targets) {
      if (target.day < day) {
        if (isNullOrUndefined(out)) {
          out = target;
        } else if (target.day > out.day) {
          out = target;
        }
      }
    }
    return out;
  }

  reloadChart() {
    if (this.chart !== undefined) {
      this.chart.chart.destroy();
      this.chart.chart = 0;

      this.chart.datasets = this.lineChartData;
      this.chart.labels = this.lineChartLabels;
      this.chart.ngOnInit();
    }
  }

  delete(target: Target) {
    for (const history of this.targetHistories) {
      const index = history.targets.indexOf(target);
      if (index > -1) {
        history.targets.splice(index, 1);
        break;
      }
    }

    this.filter();
  }

  addTarget() {
    // We're going to create and add the target here; obviously this is an atrocity. But this is a demo, so GFY.
    const nt = new Target();
    nt.client = this.selectedClient;
    nt.site = this.selectedSite;
    nt.lob = this.selectedLOB;
    nt.kpi = this.selectedKPI;
    nt.day = this.newTargetDay;
    nt.target = this.newTarget;

    for (const history of this.targetHistories) {
      if (history.client === nt.client &&
        history.site === nt.site &&
        history.lob === nt.lob &&
        history.kpi === nt.kpi) {
        history.targets.push(nt);
      }
    }

    // Clear entries
    this.newTargetDay = null;
    this.newTarget = null;

    // Filter
    this.filter();
  }

  isValid() {
    if (isNullOrUndefined(this.newTargetDay)) {
      return false;
    }
    if (isNullOrUndefined(this.newTarget)) {
      return false;
    }
    return this.newTargetDay > 0 && this.newTarget > 0;
  }
}
