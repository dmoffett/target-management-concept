import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {TargetsTableComponent} from './targets-table/targets-table.component';
import {TargetsService} from './services/targets.service';
import {MdButtonModule, MdSelectModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';
import {ChartsModule} from 'ng2-charts';

@NgModule({
  declarations: [
    AppComponent,
    TargetsTableComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    MdSelectModule,
    MdButtonModule,
    ChartsModule
  ],
  providers: [TargetsService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
